class Config(object):
    DEBUG = False
    TESTING = False
    DATABASE_URI = 'DEV_DATABASE_URI:'

class ProductionConfig(Config):
    DATABASE_URI = 'PROD_DATABASE_URI'

class DevelopmentConfig(Config):
    DEBUG = True
