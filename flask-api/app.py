from flask import Flask, jsonify

app = Flask(__name__, instance_relative_config=True)

app.config.from_object('config.DevelopmentConfig')
app.config.from_pyfile('app.cfg', silent=True)

tasks = [
    {
        'id': 1,
        'title': u'Buy groceries',
        'description': u'Milk, Cheese, Pizza, Fruit, Tylenol',
        'done': False
    },
    {
        'id': 2,
        'title': u'Learn Python',
        'description': u'Need to find a good Python tutorial on the web',
        'done': False
    }
]

@app.route('/api/task', methods=['GET'])
def get_tasks():
    app.logger.info("ENV = {}".format(app.env))
    app.logger.info("DATABASE_URI = {}".format(app.config['DATABASE_URI']))
    return jsonify({'tasks': tasks})

if __name__ == '__main__':
    app.run()
